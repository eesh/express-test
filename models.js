'use strict'

const contentful = require('contentful')

const SPACE_ID = 'aetbui3cjc0i'
const ACCESS_TOKEN = '2bee7cf21e5ea4ee168a4d8be8231b05b927ef52df8a0639efc432491c73924d'

const client = contentful.createClient({
    // This is the space ID. A space is like a project folder in Contentful terms
    space: SPACE_ID,
    // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
    accessToken: ACCESS_TOKEN
})

// Load all Content Types in your space from Contentful
function fetchContentTypes () {
  return client.getContentTypes()
  .then((response) => response.items)
  .catch((error) => {
    console.log(chalk.red('\nError occurred while fetching Content Types:'))
    console.error(error)
  })
}

// Load all entries for a given Content Type from Contentful
function fetchEntriesForContentType (contentType) {
  return client.getEntries({
      content_type: contentType.sys.id
    })
  .then((response) => response.items)
  .catch((error) => {
    console.log(chalk.red(`\nError occurred while fetching Entries for ${chalk.cyan(contentType.name)}:`))
    console.error(error)
  })
}

// Loads all asset data for a given entries id
function getEntryData (entry) {
  return client.getEntries({
        'sys.id': entry
    })
    .then((response) => response.items)
    .catch((error) => {
        console.log(chalk.red(`\nError occurred while fetching Entries Data for ${chalk.cyan(entry.sys.id)}:`))
        console.error(error)
    })
}

module.exports.client = client;
module.exports.fetchContentTypes = fetchContentTypes;
module.exports.fetchEntriesForContentType = fetchEntriesForContentType;
module.exports.getEntryData = getEntryData;
