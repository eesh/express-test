const models = require('./models')

var wait = ms => new Promise((r, j)=>setTimeout(r, ms))

module.exports.runBoilerplate = function () {
  getContentTypes()
  .then(displayEntries)
  .then((obj) => {
    // console.log(obj)
  })
  .catch((error) => {
    console.log('\nError occurred:')
    if (error.stack) {
      console.error(error.stack)
      return
    }
    console.error(error)
  })
}

function getContentTypes () {
  return models.fetchContentTypes()
  .then((contentTypes) => {
    return contentTypes
  })
}

function displayEntries (contentTypes) {
  return Promise.all(contentTypes.map((contentType) => {
    return models.fetchEntriesForContentType(contentType)
    .then((entries) => {
        if(contentType.sys.id == 'pospTraining'){
            var obj = pospTraining(entries)
            var prom = wait(2000)
            prom.then(() => {
                console.log(obj)
            })
            // console.log(obj)
        }
    })
    }))
}

function pospTraining (entries) {
    entries.forEach((entry) => {
        return models.getEntryData(entry.sys.id)
        .then(response => {
            var obj= {
                title: response[0].fields.title,
                description: response[0].fields.description,
                lesson: {
                    title: response[0].fields.lesson[0].fields.title,
                    description: response[0].fields.lesson[0].fields.description,
                    youtubeLink: response[0].fields.lesson[0].fields.youtubeVideoLink,
                    file: {
                        title: response[0].fields.lesson[0].fields.module[0].fields.title,
                        url: response[0].fields.lesson[0].fields.module[0].fields.file.url
                    }
                }
            }
            return obj
        })
        .catch((error) => {
            console.error(error)
        })
        .then((obj) => {
            console.log(obj)
            return obj
        })
    })
}
